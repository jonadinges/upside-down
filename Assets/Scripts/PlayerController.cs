﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(ScreenOrientation))]
public class PlayerController : MonoBehaviour

{
    
    private int walkable;
    [SerializeField]private NavMeshAgent downAgent,upAgent;
    ScreenOrientation screenOrientation;

    
    void Start()
    {
        walkable = LayerMask.NameToLayer("Walkable");       
        screenOrientation = transform.GetComponent<ScreenOrientation>();
    }
    


    void Update()
    {
        RaycastHit hit;

       
        if (screenOrientation.UpOrientation)  downAgent.enabled = false;
        else upAgent.enabled = false;


        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Began)
            {
                // Construct a ray from the current touch coordinates
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                
                
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {

                    if( screenOrientation.UpOrientation && hit.transform.gameObject.layer == walkable)         
                    {
                        upAgent.enabled = true;
                        upAgent.SetDestination(hit.point);
                     
                    }
                    else if (!screenOrientation.UpOrientation &&  hit.transform.gameObject.layer == walkable)
                    {

                        downAgent.enabled = true;
                        downAgent.SetDestination(hit.point);
                    
                    }
                    else
                    {
                       // Debug.Log("Other");
                    }
                }
            }
        }
    }
}
