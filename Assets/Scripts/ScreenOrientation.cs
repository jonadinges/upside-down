﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenOrientation : MonoBehaviour
{
    private string[] orientation = {"Portrait","PortraitUpsideDown" };
    [SerializeField] private Material upsideDown;
    [SerializeField] private float speed ;
    [SerializeField] public GameObject level;
    private Quaternion lookRotation;
    [SerializeField] public Vector3 upOrientation,downOrientation;
    [SerializeField] private float transitionTime =2f;

    [SerializeField] private Color upColor = new Color (1,1,1);
    [SerializeField] private Color downColor = new Color(.2f, .2f, .2f);
    private Quaternion upRot, downRot;
    public bool UpOrientation { get; set; }



    void Start()
    {
        upRot = Quaternion.Euler(upOrientation);
        downRot = Quaternion.Euler(downOrientation);
       
    }

    void Update()
    {

       

        if (Input.deviceOrientation.ToString() == orientation[0])
        {
            UpOrientation = true;
            //fade Colors from Down to Up
            upsideDown.color = Color.Lerp(upsideDown.color, upColor, transitionTime * Time.deltaTime);

            level.transform.rotation = Quaternion.Lerp(level.transform.rotation, upRot, transitionTime * Time.deltaTime);
        }
        else if (Input.deviceOrientation.ToString() == orientation[1])
        {

            UpOrientation = false;
            //fade Colors from Up to Down
            upsideDown.color = Color.Lerp(upsideDown.color, downColor, transitionTime * Time.deltaTime);

            //rotate us over time according to speed until we are in the required rotation
            level.transform.rotation = Quaternion.Slerp (level.transform.rotation, downRot, transitionTime * Time.deltaTime);
           

        }
    }
}
